package com.vrcjlb.jhcloud.repository;
import com.vrcjlb.jhcloud.domain.PartyAttribute;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PartyAttribute entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PartyAttributeRepository extends JpaRepository<PartyAttribute, Long> {

}
