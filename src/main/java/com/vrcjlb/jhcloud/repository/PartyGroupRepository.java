package com.vrcjlb.jhcloud.repository;
import com.vrcjlb.jhcloud.domain.PartyGroup;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PartyGroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PartyGroupRepository extends JpaRepository<PartyGroup, Long> {

}
