/**
 * View Models used by Spring MVC REST controllers.
 */
package com.vrcjlb.jhcloud.web.rest.vm;
